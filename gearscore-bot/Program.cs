﻿using System;
using System.Configuration;
using System.Threading;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using Discord.WebSocket;
using gearscore_bot.Model;
using gearscore_bot.Modules;
using Microsoft.Extensions.DependencyInjection;

namespace gearscore_bot
{
    class Program
    {
        static void Main(string[] args)
            => new Program().RunBotAsync().GetAwaiter().GetResult();

        private DiscordSocketClient _client;
        private CommandService _commands;
        private IServiceProvider _services;
        private CancellationTokenSource _tokenSource;

        public async Task RunBotAsync()
        {
            if(!System.IO.File.Exists(LookupTables.ClassImages[1]))
            {
                Console.WriteLine($"Cannot find image files.{LookupTables.ClassImages[1]}");
                Environment.Exit(-1);
            }

            _tokenSource = new CancellationTokenSource();
            _commands = new CommandService();
            _client = new DiscordSocketClient();
            _services = BuildServiceProvider();

            _client.Log += Log;
            _client.UserJoined += UserJoined;
            _client.UserLeft += UserLeft;

            CommandHandler ch = new CommandHandler(_client, _commands, _services);
            await ch.InstallCommandsAsync();
            await _client.LoginAsync(TokenType.Bot, ConfigurationManager.AppSettings["token"]);
            await _client.StartAsync();
            await Task.Delay(-1);
        }

        private async Task UserLeft(SocketGuildUser user)
        {
            Console.WriteLine("User left");
            EmbedBuilder eb = new EmbedBuilder();
            eb.WithTitle($"{user.Username} ({user.Nickname}) has left the server.");
            eb.AddField(new EmbedFieldBuilder()
            {
                Name = "Member since",
                Value = user.JoinedAt.HasValue ? user.JoinedAt.Value.ToLocalTime().ToString("g") : "Unknown",
                IsInline = true
            });
            eb.WithColor(Color.Red);
            eb.WithThumbnailUrl(user.GetAvatarUrl(size: 64) ?? user.GetDefaultAvatarUrl());
            eb.WithCurrentTimestamp();
            await _client.GetGuild(758050738026053743).GetTextChannel(851198676730380368).SendMessageAsync(embed: eb.Build());
        }

        private async Task UserJoined(SocketGuildUser user)
        {
            Console.WriteLine("User Joined");
            EmbedBuilder eb = new EmbedBuilder();
            eb.WithTitle($"{user.Username}{(string.IsNullOrEmpty(user.Nickname) ? "" : $" ({user.Nickname})")} has joined the server.");
            eb.WithColor(Color.Green);
            eb.WithThumbnailUrl(user.GetAvatarUrl(size: 64) ?? user.GetDefaultAvatarUrl());
            eb.WithCurrentTimestamp();
            await _client.GetGuild(758050738026053743).GetTextChannel(851198676730380368).SendMessageAsync(embed: eb.Build());
        }

        private Task Log(LogMessage msg)
        {
            Console.WriteLine(msg.ToString());
            return Task.CompletedTask;
        }

        public IServiceProvider BuildServiceProvider() => new ServiceCollection()
            .AddSingleton(_client)
            .AddSingleton(_commands)
            .AddSingleton<GearscoreCalculator>()
            .AddSingleton<GuildLog>(new GuildLog(_client, _tokenSource.Token))
            .BuildServiceProvider();
    }
}
