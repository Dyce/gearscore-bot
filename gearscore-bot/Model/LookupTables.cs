﻿using System;
using System.Collections.Generic;
using System.IO;
using Discord;

namespace gearscore_bot.Model
{
    public static class LookupTables
    {
        public static readonly Dictionary<int, string> ClassImages = new Dictionary<int, string>()
        {
            { 1, Path.Combine(AppDomain.CurrentDomain.BaseDirectory, $"Model{Path.DirectorySeparatorChar}Images{Path.DirectorySeparatorChar}warrior48.png")},
            { 2, Path.Combine(AppDomain.CurrentDomain.BaseDirectory, $"Model{Path.DirectorySeparatorChar}Images{Path.DirectorySeparatorChar}paladin48.png")},
            { 4, Path.Combine(AppDomain.CurrentDomain.BaseDirectory, $"Model{Path.DirectorySeparatorChar}Images{Path.DirectorySeparatorChar}hunter48.png")},
            { 8, Path.Combine(AppDomain.CurrentDomain.BaseDirectory, $"Model{Path.DirectorySeparatorChar}Images{Path.DirectorySeparatorChar}rogue48.png")},
            { 16, Path.Combine(AppDomain.CurrentDomain.BaseDirectory, $"Model{Path.DirectorySeparatorChar}Images{Path.DirectorySeparatorChar}priest48.png")},
            { 32, Path.Combine(AppDomain.CurrentDomain.BaseDirectory, $"Model{Path.DirectorySeparatorChar}Images{Path.DirectorySeparatorChar}dk48.png")},
            { 64, Path.Combine(AppDomain.CurrentDomain.BaseDirectory, $"Model{Path.DirectorySeparatorChar}Images{Path.DirectorySeparatorChar}shaman48.png")},
            { 128, Path.Combine(AppDomain.CurrentDomain.BaseDirectory, $"Model{Path.DirectorySeparatorChar}Images{Path.DirectorySeparatorChar}mage48.png")},
            { 256, Path.Combine(AppDomain.CurrentDomain.BaseDirectory, $"Model{Path.DirectorySeparatorChar}Images{Path.DirectorySeparatorChar}warlock48.png")},
            { 1024, Path.Combine(AppDomain.CurrentDomain.BaseDirectory, $"Model{Path.DirectorySeparatorChar}Images{Path.DirectorySeparatorChar}druid48.png")}
        };

        public static readonly Dictionary<int, Color> ClassColors = new Dictionary<int, Color>()
        {
            { 1, new Color(198, 155, 109)},
            { 2, new Color(244, 140, 186)},
            { 4, new Color(170, 211, 114)},
            { 8, new Color(255, 244, 104)},
            { 16, new Color(254, 254, 254)},
            { 32, new Color(196, 30, 59)},
            { 64, new Color(35, 89, 255)},
            { 128, new Color(104, 204, 239)},
            { 256, new Color(147, 130, 201)},
            { 1024, new Color(255, 124, 10)},
        };

        public static readonly GearscoreColorInfo[] GearscoreColors =
        {
            // <0, 1000>
            new GearscoreColorInfo(
                new ColorChannelInfo() {Base = 0.55f, Bias = 0.00045f}, //red
                new ColorChannelInfo() {Base = 0.55f, Bias = 0.00045f}, //green
                new ColorChannelInfo() {Base = 0.55f, Bias = 0.00045f} //blue
                ),
            // <1001, 2000>
            new GearscoreColorInfo(
                new ColorChannelInfo() {Base = 1f, Bias = -0.00088f},
                new ColorChannelInfo() {Base = 1f, Bias = 0f},
                new ColorChannelInfo() {Base = 1f, Bias = -0.001f}
                ),
            // <2001, 3000>
            new GearscoreColorInfo(
                new ColorChannelInfo() {Base = 0.12f, Bias = -0.00012f},
                new ColorChannelInfo() {Base = 1f, Bias = -0.00050f},
                new ColorChannelInfo() {Base = 0f, Bias = 0.001f}
                ),
            // <3001, 4000>
            new GearscoreColorInfo(
                new ColorChannelInfo() {Base = 0f, Bias = 0.00069f},
                new ColorChannelInfo() {Base = 0.5f, Bias = -0.00022f},
                new ColorChannelInfo() {Base = 1f, Bias = -0.00003f}
                ),
            // <4001, 5000>
            new GearscoreColorInfo(
                new ColorChannelInfo() {Base = 0.69f, Bias = 0.00025f},
                new ColorChannelInfo() {Base = 0.28f, Bias = 0.00019f},
                new ColorChannelInfo() {Base = 0.97f, Bias = -0.00096f}
                ),
            // <5001, 6000>
            new GearscoreColorInfo(
                new ColorChannelInfo() {Base = 0.94f, Bias = 0.00006f},
                new ColorChannelInfo() {Base = 0.47f, Bias = -0.00047f},
                new ColorChannelInfo() {Base = 0f, Bias = 0f}
                ),
            // 6001+
            new GearscoreColorInfo(
                new ColorChannelInfo() {Base = 1f, Bias = 0f},
                new ColorChannelInfo() {Base = 0f, Bias = 0f},
                new ColorChannelInfo() {Base = 0f, Bias = 0f}
                )
        };

        public struct GearscoreColorInfo
        {
            public GearscoreColorInfo(ColorChannelInfo red, ColorChannelInfo green, ColorChannelInfo blue)
            {
                Red = red;
                Green = green;
                Blue = blue;
            }
            public readonly ColorChannelInfo Red;
            public readonly ColorChannelInfo Green;
            public readonly ColorChannelInfo Blue;
        }

        public struct ColorChannelInfo
        {
            public float Base;
            public float Bias;
        }
    }
}
