﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace gearscore_bot.Model.Website
{
    public class CharacterSummary
    {
        [JsonProperty("error")]
        public string Error { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("realm")]
        public string Realm { get; set; }

        [JsonProperty("online")]
        public bool Online { get; set; }

        [JsonProperty("level")]
        public string Level { get; set; }

        [JsonProperty("faction")]
        public string Faction { get; set; }

        [JsonProperty("gender")]
        public string Gender { get; set; }

        [JsonProperty("race")]
        public string Race { get; set; }

        [JsonProperty("racemask")]
        public int RaceMask { get; set; }

        [JsonProperty("class")]
        public string Class { get; set; }

        [JsonProperty("classmask")]
        public int ClassMask { get; set; }

        [JsonProperty("honorablekills")]
        public string HonorableKills { get; set; }

        [JsonProperty("guild")]
        public string Guild { get; set; }

        [JsonProperty("achievementpoints")]
        public string AchievementPoints { get; set; }

        [JsonProperty("equipment")]
        public List<Equipment> Equipment { get; set; }

        [JsonProperty("talents")]
        public List<Talent> Talents { get; set; }

        [JsonProperty("pvpteams")]
        public List<PvpTeam> PvpTeams { get; set; }

        [JsonProperty("professions")]
        public List<Profession> Professions { get; set; }

    }

    public class Equipment
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("item")]
        public string Item { get; set; }

        [JsonProperty("transmog")]
        public string Transmog { get; set; }
    }

    public class Talent
    {
        [JsonProperty("tree")]
        public string Tree { get; set; }

        [JsonProperty("points")]
        public int[] Points { get; set; }
    }

    public class Profession
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("skill")]
        public string Skill { get; set; }
    }

    public class PvpTeam
    {
        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("rating")]
        public string Rating { get; set; }

        [JsonProperty("rank")]
        public string Rank { get; set; }
    }
}
