﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace gearscore_bot.Model.Website
{
    public class GuildSummary
    {
        [JsonProperty("error")]
        public string Error { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("realm")]
        public string Realm { get; set; }
        [JsonProperty("membercount")]
        public string MemberCount { get; set; }
        [JsonProperty("pvepoints")]
        public string PvEPoints { get; set; }
        [JsonProperty("leader")]
        public Leader Leader { get; set; }
        [JsonProperty("roster")]
        public List<Member> Roster { get; set; }
    }

    public class Leader
    {
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("level")]
        public string Level { get; set; }
        [JsonProperty("gender")]
        public string Gender { get; set; }
        [JsonProperty("Race")]
        public string Race { get; set; }
        [JsonProperty("racemask")]
        public int RaceMask { get; set; }
        [JsonProperty("class")]
        public string Class { get; set; }
        [JsonProperty("classmask")]
        public int ClassMask { get; set; }
    }

    public class Member
    {
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("online")]
        public bool Online { get; set; }
        [JsonProperty("level")]
        public string Level { get; set; }
        [JsonProperty("gender")]
        public string Gender { get; set; }
        [JsonProperty("race")]
        public string Race { get; set; }
        [JsonProperty("racemask")]
        public int RaceMask { get; set; }
        [JsonProperty("class")]
        public string Class { get; set; }
        [JsonProperty("classmask")]
        public int ClassMask { get; set; }
        [JsonProperty("achievementpoints")]
        public string Achievementpoints { get; set; }
        //[JsonProperty("professions")]
        //public Professions Professions { get; set; }
    }

    //public class Professions
    //{
    //    [JsonProperty("professions")]
    //    public List<Profession> professions { get; set; }
    //}
}
