﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using Microsoft.EntityFrameworkCore;

namespace gearscore_bot.Model.Database
{
    public class WarcraftContext : DbContext
    {
        public DbSet<WowItem> WowItems { get; set; }
        public DbSet<WowBondingType> WowBondingTypes { get; set; }
        public DbSet<WowInventorySlot> WowInventorySlots { get; set; }
        public DbSet<WowItemClass> WowItemClasses { get; set; }
        public DbSet<WowItemSubclass> WowItemSubclasses { get; set; }
        public DbSet<WowItemQuality> WowItemQualities { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite("Data Source=botdata.db");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<WowItemSubclass>().HasKey(k => new { k.ClassId, k.SubclassId });

            modelBuilder.Entity<WowItemClass>()
                .HasData(new WowItemClass { Id = 0, Name = "Consumable" },
                new WowItemClass { Id = 1, Name = "Container" },
                new WowItemClass { Id = 2, Name = "Weapon" },
                new WowItemClass { Id = 3, Name = "Gem" },
                new WowItemClass { Id = 4, Name = "Armor" },
                new WowItemClass { Id = 5, Name = "Reagent" },
                new WowItemClass { Id = 6, Name = "Projectile" },
                new WowItemClass { Id = 7, Name = "Trade Goods" },
                new WowItemClass { Id = 8, Name = "Generic" },
                new WowItemClass { Id = 9, Name = "Recipe" },
                new WowItemClass { Id = 10, Name = "Money" },
                new WowItemClass { Id = 11, Name = "Quiver" },
                new WowItemClass { Id = 12, Name = "Quest" },
                new WowItemClass { Id = 13, Name = "Key" },
                new WowItemClass { Id = 14, Name = "Permanent" },
                new WowItemClass { Id = 15, Name = "Miscellaneous" },
                new WowItemClass { Id = 16, Name = "Glyph" }
                );

            modelBuilder.Entity<WowItemSubclass>().HasData(
                new { ClassId = 0, SubclassId = 0, Name = "Consumable" },
                new { ClassId = 0, SubclassId = 1, Name = "Potion" },
                new { ClassId = 0, SubclassId = 2, Name = "Elixir" },
                new { ClassId = 0, SubclassId = 3, Name = "Flask" },
                new { ClassId = 0, SubclassId = 4, Name = "Scroll" },
                new { ClassId = 0, SubclassId = 5, Name = "Food & Drink" },
                new { ClassId = 0, SubclassId = 6, Name = "Item Enhancement" },
                new { ClassId = 0, SubclassId = 7, Name = "Bandage" },
                new { ClassId = 0, SubclassId = 8, Name = "Other" },
                new { ClassId = 1, SubclassId = 0, Name = "Bag" },
                new { ClassId = 1, SubclassId = 1, Name = "Soul Bag" },
                new { ClassId = 1, SubclassId = 2, Name = "Herb Bag" },
                new { ClassId = 1, SubclassId = 3, Name = "Enchanting Bag" },
                new { ClassId = 1, SubclassId = 4, Name = "Engineering Bag" },
                new { ClassId = 1, SubclassId = 5, Name = "Gem Bag" },
                new { ClassId = 1, SubclassId = 6, Name = "Mining Bag" },
                new { ClassId = 1, SubclassId = 7, Name = "Leatherworking Bag" },
                new { ClassId = 1, SubclassId = 8, Name = "Inscription Bag" },
                new { ClassId = 2, SubclassId = 0, Name = "One Hand Axe" },
                new { ClassId = 2, SubclassId = 1, Name = "Two Hand Axe" },
                new { ClassId = 2, SubclassId = 2, Name = "Bow" },
                new { ClassId = 2, SubclassId = 3, Name = "Gun" },
                new { ClassId = 2, SubclassId = 4, Name = "One Hand Mace" },
                new { ClassId = 2, SubclassId = 5, Name = "Two Hand Mace" },
                new { ClassId = 2, SubclassId = 6, Name = "Polearm" },
                new { ClassId = 2, SubclassId = 7, Name = "One Hand Sword" },
                new { ClassId = 2, SubclassId = 8, Name = "Two Hand Sword" },
                new { ClassId = 2, SubclassId = 9, Name = "Obsolete" },
                new { ClassId = 2, SubclassId = 10, Name = "Staff" },
                new { ClassId = 2, SubclassId = 11, Name = "Exotic" },
                new { ClassId = 2, SubclassId = 12, Name = "Exotic" },
                new { ClassId = 2, SubclassId = 13, Name = "Fist Weapon" },
                new { ClassId = 2, SubclassId = 14, Name = "Miscellaneous" },
                new { ClassId = 2, SubclassId = 15, Name = "Dagger" },
                new { ClassId = 2, SubclassId = 16, Name = "Thrown" },
                new { ClassId = 2, SubclassId = 17, Name = "Spear" },
                new { ClassId = 2, SubclassId = 18, Name = "Crossbow" },
                new { ClassId = 2, SubclassId = 19, Name = "Wand" },
                new { ClassId = 2, SubclassId = 20, Name = "Fishing Pole" },
                new { ClassId = 3, SubclassId = 0, Name = "Red" },
                new { ClassId = 3, SubclassId = 1, Name = "Blue" },
                new { ClassId = 3, SubclassId = 2, Name = "Yellow" },
                new { ClassId = 3, SubclassId = 3, Name = "Purple" },
                new { ClassId = 3, SubclassId = 4, Name = "Green" },
                new { ClassId = 3, SubclassId = 5, Name = "Orange" },
                new { ClassId = 3, SubclassId = 6, Name = "Meta" },
                new { ClassId = 3, SubclassId = 7, Name = "Simple" },
                new { ClassId = 3, SubclassId = 8, Name = "Prismatic" },
                new { ClassId = 4, SubclassId = 0, Name = "Miscellaneous" },
                new { ClassId = 4, SubclassId = 1, Name = "Cloth" },
                new { ClassId = 4, SubclassId = 2, Name = "Leather" },
                new { ClassId = 4, SubclassId = 3, Name = "Mail" },
                new { ClassId = 4, SubclassId = 4, Name = "Plate" },
                new { ClassId = 4, SubclassId = 5, Name = "Buckler" },
                new { ClassId = 4, SubclassId = 6, Name = "Shield" },
                new { ClassId = 4, SubclassId = 7, Name = "Libram" },
                new { ClassId = 4, SubclassId = 8, Name = "Idol" },
                new { ClassId = 4, SubclassId = 9, Name = "Totem" },
                new { ClassId = 4, SubclassId = 10, Name = "Sigil" },
                new { ClassId = 5, SubclassId = 0, Name = "Reagent" },
                new { ClassId = 6, SubclassId = 0, Name = "Wand" },
                new { ClassId = 6, SubclassId = 1, Name = "Bolt" },
                new { ClassId = 6, SubclassId = 2, Name = "Arrow" },
                new { ClassId = 6, SubclassId = 3, Name = "Bullet" },
                new { ClassId = 6, SubclassId = 4, Name = "Thrown" },
                new { ClassId = 7, SubclassId = 0, Name = "Trade Goods" },
                new { ClassId = 7, SubclassId = 1, Name = "Parts" },
                new { ClassId = 7, SubclassId = 2, Name = "Explosives" },
                new { ClassId = 7, SubclassId = 3, Name = "Devices" },
                new { ClassId = 7, SubclassId = 4, Name = "Jewelcrafting" },
                new { ClassId = 7, SubclassId = 5, Name = "Cloth" },
                new { ClassId = 7, SubclassId = 6, Name = "Leather" },
                new { ClassId = 7, SubclassId = 7, Name = "Metal & Stone" },
                new { ClassId = 7, SubclassId = 8, Name = "Meat" },
                new { ClassId = 7, SubclassId = 9, Name = "Herb" },
                new { ClassId = 7, SubclassId = 10, Name = "Elemental" },
                new { ClassId = 7, SubclassId = 11, Name = "Other" },
                new { ClassId = 7, SubclassId = 12, Name = "Enchanting" },
                new { ClassId = 7, SubclassId = 13, Name = "Materials" },
                new { ClassId = 7, SubclassId = 14, Name = "Armor Enchantment" },
                new { ClassId = 7, SubclassId = 15, Name = "Wapon Enchantment" },
                new { ClassId = 8, SubclassId = 0, Name = "Generic" },
                new { ClassId = 9, SubclassId = 0, Name = "Book" },
                new { ClassId = 9, SubclassId = 1, Name = "Leatherworking" },
                new { ClassId = 9, SubclassId = 2, Name = "Tailoring" },
                new { ClassId = 9, SubclassId = 3, Name = "Engineering" },
                new { ClassId = 9, SubclassId = 4, Name = "Blacksmithing" },
                new { ClassId = 9, SubclassId = 5, Name = "Cooking" },
                new { ClassId = 9, SubclassId = 6, Name = "Alchemy" },
                new { ClassId = 9, SubclassId = 7, Name = "First Aid" },
                new { ClassId = 9, SubclassId = 8, Name = "Enchanting" },
                new { ClassId = 9, SubclassId = 9, Name = "Fishing" },
                new { ClassId = 9, SubclassId = 10, Name = "Jewelcrafting" },
                new { ClassId = 10, SubclassId = 0, Name = "Money" },
                new { ClassId = 11, SubclassId = 0, Name = "Quiver" },
                new { ClassId = 11, SubclassId = 1, Name = "Quiver" },
                new { ClassId = 11, SubclassId = 2, Name = "Quiver" },
                new { ClassId = 11, SubclassId = 3, Name = "Ammo Pouch" },
                new { ClassId = 12, SubclassId = 0, Name = "Quest" },
                new { ClassId = 13, SubclassId = 0, Name = "Key" },
                new { ClassId = 13, SubclassId = 1, Name = "Lockpick" },
                new { ClassId = 14, SubclassId = 0, Name = "Permanent" },
                new { ClassId = 15, SubclassId = 0, Name = "Junk" },
                new { ClassId = 15, SubclassId = 1, Name = "Reagent" },
                new { ClassId = 15, SubclassId = 2, Name = "Pet" },
                new { ClassId = 15, SubclassId = 3, Name = "Holiday" },
                new { ClassId = 15, SubclassId = 4, Name = "Other" },
                new { ClassId = 15, SubclassId = 5, Name = "Mount" },
                new { ClassId = 16, SubclassId = 1, Name = "Warrior" },
                new { ClassId = 16, SubclassId = 2, Name = "Paladin" },
                new { ClassId = 16, SubclassId = 3, Name = "Hunter" },
                new { ClassId = 16, SubclassId = 4, Name = "Rogue" },
                new { ClassId = 16, SubclassId = 5, Name = "Priet" },
                new { ClassId = 16, SubclassId = 6, Name = "Death Knight" },
                new { ClassId = 16, SubclassId = 7, Name = "Shaman" },
                new { ClassId = 16, SubclassId = 8, Name = "Mage" },
                new { ClassId = 16, SubclassId = 9, Name = "Warlock" },
                new { ClassId = 16, SubclassId = 11, Name = "Druid" }
                );

            modelBuilder.Entity<WowInventorySlot>().HasData(
                new WowInventorySlot { Id = 0, Name = "Non-equippable" },
                new WowInventorySlot { Id = 1, Name = "Head" },
                new WowInventorySlot { Id = 2, Name = "Neck" },
                new WowInventorySlot { Id = 3, Name = "Shoulder" },
                new WowInventorySlot { Id = 4, Name = "Shirt" },
                new WowInventorySlot { Id = 5, Name = "Chest" },
                new WowInventorySlot { Id = 6, Name = "Waist" },
                new WowInventorySlot { Id = 7, Name = "Legs" },
                new WowInventorySlot { Id = 8, Name = "Feet" },
                new WowInventorySlot { Id = 9, Name = "Wrists" },
                new WowInventorySlot { Id = 10, Name = "Hands" },
                new WowInventorySlot { Id = 11, Name = "Finger" },
                new WowInventorySlot { Id = 12, Name = "Trinket" },
                new WowInventorySlot { Id = 13, Name = "Weapon" },
                new WowInventorySlot { Id = 14, Name = "Shield" },
                new WowInventorySlot { Id = 15, Name = "Ranged" }, //Bows
                new WowInventorySlot { Id = 16, Name = "Back" },
                new WowInventorySlot { Id = 17, Name = "Two-Hand" },
                new WowInventorySlot { Id = 18, Name = "Bag" },
                new WowInventorySlot { Id = 19, Name = "Tabbard" },
                new WowInventorySlot { Id = 20, Name = "Robe" },
                new WowInventorySlot { Id = 21, Name = "Main Hand" },
                new WowInventorySlot { Id = 22, Name = "Off Hand" },
                new WowInventorySlot { Id = 23, Name = "Holdable" }, //Tome
                new WowInventorySlot { Id = 24, Name = "Ammo" },
                new WowInventorySlot { Id = 25, Name = "Thrown" },
                new WowInventorySlot { Id = 26, Name = "Ranged right" }, //wand,gun
                new WowInventorySlot { Id = 27, Name = "Quiver" },
                new WowInventorySlot { Id = 28, Name = "Relic" }
                );

            modelBuilder.Entity<WowBondingType>().HasData(
                new WowBondingType { Id = 0, Name = "No bonds" },
                new WowBondingType { Id = 1, Name = "Binds when picked up" },
                new WowBondingType { Id = 2, Name = "Binds when equipped" },
                new WowBondingType { Id = 3, Name = "Binds when used" },
                new WowBondingType { Id = 4, Name = "Quest item" },
                new WowBondingType { Id = 5, Name = "Quest item1" }
                );

            modelBuilder.Entity<WowItemQuality>().HasData(
                new WowItemQuality { Id = 0, Color = Color.Gray.ToArgb(), Name = "Poor" },
                new WowItemQuality { Id = 1, Color = Color.White.ToArgb(), Name = "Common" },
                new WowItemQuality { Id = 2, Color = Color.Green.ToArgb(), Name = "Uncommon" },
                new WowItemQuality { Id = 3, Color = Color.Blue.ToArgb(), Name = "Rare" },
                new WowItemQuality { Id = 4, Color = Color.Purple.ToArgb(), Name = "Epic" },
                new WowItemQuality { Id = 5, Color = Color.Orange.ToArgb(), Name = "Legendary" },
                new WowItemQuality { Id = 6, Color = Color.Red.ToArgb(), Name = "Artifact" },
                new WowItemQuality { Id = 7, Color = Color.Gold.ToArgb(), Name = "Bind to Account" }
                );

            base.OnModelCreating(modelBuilder);
        }
    }
}
