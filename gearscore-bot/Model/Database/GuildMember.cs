﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace gearscore_bot.Model.Database
{
    [Table("roster")]
    public class GuildMember
    {
        public string Name { get; set; }
        public int Level { get; set; }
        public string Gender { get; set; }
        public string Race { get; set; }
        public int RaceMask { get; set; }
        public string Class { get; set; }
        public int ClassMask { get; set; }
        public int Achievementpoints { get; set; }
        public bool IsMember { get; set; }
        public List<GuildLogEntry> Logs { get; set; }
    }
}
