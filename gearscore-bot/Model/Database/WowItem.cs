﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace gearscore_bot.Model.Database
{
    [Table("wow_items")]
    public class WowItem
    {
        [Key]
        [Column("entry", Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int EntryId { get; set; }

        [Column("class", Order = 1)]
        public int ClassId { get; set; }

        [Column("subclass", Order = 2)]
        public int SubclassId { get; set; }

        [Column("name", Order = 3)]
        public string Name { get; set; }

        [Column("displayid", Order = 4)]
        public int? DisplayId { get; set; }

        [Column("Quality", Order = 5)]
        public int? QualityId { get; set; }

        [Column("Flags", Order = 6)]
        public int? Flags { get; set; }

        [Column("FlagsExtra", Order = 7)]
        public int? FlagsExtra { get; set; }

        [Column("InventoryType", Order = 8)]
        public int? InventoryTypeId { get; set; }

        [Column("ItemLevel", Order = 9)]
        public int? ItemLevel { get; set; }

        [Column("bonding", Order = 10)]
        public int? BondingId { get; set; }

        [Column("description", Order = 11)]
        public string Description { get; set; }

        [Column("socketColor_1", Order = 12)]
        public int FirstSocketType { get; set; }

        [Column("socketColor_2", Order = 13)]
        public int SecondSocketType { get; set; }

        [Column("socketColor_3", Order = 14)]
        public int ThirdSocketType { get; set; }

        [Column("socketBonus", Order = 15)]
        public int SocketBonusId { get; set; }

        [ForeignKey(nameof(ClassId))]
        public virtual WowItemClass ItemClass { get; set; }
        
        [ForeignKey("ClassId,SubclassId")]
        public virtual WowItemSubclass ItemSubclass { get; set; }

        [ForeignKey(nameof(InventoryTypeId))]
        public virtual WowInventorySlot InventorySlot { get; set; }

        [ForeignKey(nameof(BondingId))]
        public virtual WowBondingType BondingType { get; set; }

        [ForeignKey(nameof(QualityId))]
        public virtual WowItemQuality Quality { get; set; }
    }
}
