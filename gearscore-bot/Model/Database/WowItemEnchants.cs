﻿using System;
using System.Collections.Generic;
using System.Text;

namespace gearscore_bot.Model.Database
{
    public class WowItemEnchants
    {
        public int ItemId { get; set; }
        public int EnchantId { get; set; }
        public int[] Gems { get; set; }
        public int TransmogItemId { get; set; }
    }
}
