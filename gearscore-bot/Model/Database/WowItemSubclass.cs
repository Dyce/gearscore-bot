﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace gearscore_bot.Model.Database
{
    [Table("wow_itemsubclasses")]
    public class WowItemSubclass
    {
        [Column("class_id", Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ClassId { get; set; }

        [Column("subclass_id", Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int SubclassId { get; set; }

        [Column("name", Order = 2)]
        public string Name { get; set; }

        [ForeignKey(nameof(ClassId))]
        public WowItemClass ItemClass { get; set; }
    }
}
