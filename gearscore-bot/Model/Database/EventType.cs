﻿using System;
using System.Collections.Generic;
using System.Text;

namespace gearscore_bot.Model.Database
{
    public class EventType
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
