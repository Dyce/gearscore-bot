﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;

namespace gearscore_bot.Model.Database
{
    public class GuildContext : DbContext
    {
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite("Data Source=guild.db");
        }

        public DbSet<GuildMember> Members { get; set; }
        public DbSet<GuildLogEntry> Logs { get; set; }
        public DbSet<EventType> EventTypes { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<GuildMember>().HasKey(k => k.Name);
            
            modelBuilder.Entity<EventType>().HasKey(k => k.Id);
            modelBuilder.Entity<EventType>().HasData(
                new EventType() {Id = 1, Name = "Joined Guild"},
                new EventType() { Id = 2, Name = "Left Guild" }
                );

            modelBuilder.Entity<GuildLogEntry>().HasKey(k => k.Id);
            modelBuilder.Entity<GuildLogEntry>().HasOne(l => l.Member).WithMany(m => m.Logs);
            modelBuilder.Entity<GuildLogEntry>().HasOne(l => l.EventType).WithMany();
            modelBuilder.Entity<GuildLogEntry>().Property(p => p.Timestamp).HasDefaultValueSql("datetime('now', 'localtime')");


            base.OnModelCreating(modelBuilder);
        }
    }
}