﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace gearscore_bot.Model.Database
{
    public class GuildLogEntry
    {
        public int Id { get; set; }
        public GuildMember Member { get; set; }
        public EventType EventType { get; set; }
        public DateTime Timestamp { get; set; }

    }
}
