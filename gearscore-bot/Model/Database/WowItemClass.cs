﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace gearscore_bot.Model.Database
{
    [Table("wow_itemclasses")]
    public class WowItemClass
    {
        [Key]
        [Column("id",  Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }

        [Column("name", Order = 1)]
        public string Name { get; set; }
    }
}
