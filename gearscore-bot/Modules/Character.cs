﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using gearscore_bot.Model;
using gearscore_bot.Model.Database;
using gearscore_bot.Model.Website;
using HtmlAgilityPack;
using Newtonsoft.Json;

namespace gearscore_bot.Modules
{
    public class Character : ModuleBase<SocketCommandContext>
    {
        private readonly GearscoreCalculator _gscalc;

        public Character(GearscoreCalculator gsc)
        {
            _gscalc = gsc;
        }

        [Command("gs")]
        public async Task EmbedSummaryAsync(string playerName)
        {
            if(!Regex.IsMatch(playerName, @"^[a-zA-Z]+$"))
            {
                Console.WriteLine($"Invalid character name: {playerName}");
                return;
            }

            string warmaneUrl = $"http://armory.warmane.com/api/character/{System.Net.WebUtility.UrlEncode(playerName)}/Icecrown/Summary";

            CharacterSummary characterSummary;
            string response = null;
            try
            {
                HttpClient apiClient = new HttpClient();
                apiClient.DefaultRequestHeaders.Accept.Clear();
                apiClient.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                apiClient.DefaultRequestHeaders.Add("User-Agent", ".NET Core Client");
                Task<string> responseTask = apiClient.GetStringAsync(warmaneUrl);
                response = await responseTask;

                //error response: {"error":"Character does not exist."}

                characterSummary = JsonConvert.DeserializeObject<CharacterSummary>(response);

                if(characterSummary.Error != null)
                {
                    Console.WriteLine($"{characterSummary.Error}");
                    await ReplyAsync($"{characterSummary.Error}");
                    return;
                }
            }
            catch(JsonException ex)
            {
                await ReplyAsync($"Error parsing API data.");
                Console.WriteLine($"Error parsing API data ({nameof(JsonException)}, character name: {playerName}, {ex.Message})");
                return;
            }
            catch(HttpRequestException ex)
            {
                await ReplyAsync($"Error requesting API data.");
                Console.WriteLine($"Error requesting API data ({nameof(HttpRequestException)}, {ex.Message})");
                return;
            }

            int gearScore = _gscalc.CalculateGearscore(characterSummary.Equipment, characterSummary.Class);

            HttpClient client = new HttpClient();
            HtmlDocument doc = new HtmlDocument();
            HttpResponseMessage result;
            try
            {
                result = await client.GetAsync($"http://armory.warmane.com/character/{System.Net.WebUtility.UrlEncode(playerName)}/Icecrown/Summary");
                Stream stream = await result.Content.ReadAsStreamAsync();
                doc.Load(stream);
                //doc.Load(@"C:\Users\Michal\Desktop\html.txt");
            }
            catch(HttpRequestException ex)
            {
                await ReplyAsync($"Error downloading data from armory webpage.");
                Console.WriteLine($"Error downloading armory content. {nameof(HttpRequestException)}: {ex.Message}");
                return;
            }
            string[] rels = null;
            try
            {
                rels =
                    doc.DocumentNode.SelectNodes("//div[contains(concat(' ', @class, ' '), ' item-model ')]//div[contains(concat(' ', @class, ' '), ' item-slot ')]//a[@rel]")
                    .Select(x => x.GetAttributeValue("rel", "")).ToArray();
            }
            catch(ArgumentNullException ex)
            {
                //await ReplyAsync($"Error parsing HTML values {nameof(ArgumentNullException)}");
                Console.WriteLine($"Error parsing HTML values. {nameof(ArgumentNullException)}: {ex.Message}");
                return;
            }
            List<WowItemEnchants> items = new List<WowItemEnchants>();

            foreach(string rel in rels)
            {
                string[] data = rel.Split("&amp;");
                //Console.WriteLine(string.Join(" ", data));

                WowItemEnchants wowItem = new WowItemEnchants();
                wowItem.ItemId = 0;
                wowItem.EnchantId = 0;
                wowItem.TransmogItemId = 0;
                wowItem.Gems = new int[3] { 0, 0, 0 };

                foreach(string item in data)
                {
                    if(item.StartsWith("item="))
                    {
                        wowItem.ItemId = Convert.ToInt32(item.Replace("item=", ""));
                        continue;
                    }
                    if(item.StartsWith("ench="))
                    {
                        wowItem.EnchantId = Convert.ToInt32(item.Replace("ench=", ""));
                        continue;
                    }
                    if(item.StartsWith("transmog="))
                    {
                        wowItem.TransmogItemId = Convert.ToInt32(item.Replace("transmog=", ""));
                        continue;
                    }
                    if(item.StartsWith("gems="))
                    {
                        string[] gems = item.Replace("gems=", "").Split(':');
                        for(int i = 0; i < gems.Length; i++)
                        {
                            wowItem.Gems[i] = Convert.ToInt32(gems[i]);
                        }
                        continue;
                    }
                }
                items.Add(wowItem);
            }

            EmbedBuilder embedBuilder = new EmbedBuilder();
            embedBuilder.WithTitle($"Summary for character {characterSummary.Name}");

            embedBuilder.AddField(new EmbedFieldBuilder()
            {
                Name = "Details",
                Value = $"Level {characterSummary.Level} {characterSummary.Race} {characterSummary.Class}",
                IsInline = true
            });

            embedBuilder.AddField(new EmbedFieldBuilder()
            {
                Name = "Faction",
                Value = characterSummary.Faction,
                IsInline = true
            });

            //empty field
            embedBuilder.AddField(new EmbedFieldBuilder()
            {
                Name = "\u200b",
                Value = "\u200b",
                IsInline = true
            });

            embedBuilder.AddField(new EmbedFieldBuilder()
            {
                Name = "Status",
                Value = characterSummary.Online == true ? "\U0001f7e2 Online" : "\U0001F534 Offline",
                IsInline = true
            });

            embedBuilder.AddField(new EmbedFieldBuilder()
            {
                Name = characterSummary.Talents.Count == 1 ? "Spec" : "Specs",
                Value = $"{characterSummary.Talents[0].Tree}{(characterSummary.Talents.Count == 2 ? $" and {characterSummary.Talents[1].Tree}" : "")}",
                IsInline = true
            });
            
            embedBuilder.AddField(new EmbedFieldBuilder()
            {
                Name = "Guild",
                Value = string.IsNullOrWhiteSpace(characterSummary.Guild) ? "\u200b" : characterSummary.Guild,
                IsInline = true
            });

            embedBuilder.AddField(new EmbedFieldBuilder()
            {
                Name = "Gearscore",
                Value = gearScore,
                IsInline = true
            });

            embedBuilder.AddField(new EmbedFieldBuilder()
            {
                Name = "Achievement points",
                Value = characterSummary.AchievementPoints,
                IsInline = true
            });

            embedBuilder.AddField(new EmbedFieldBuilder()
            {
                Name = "Honorable kills",
                Value = characterSummary.HonorableKills,
                IsInline = true
            });

            embedBuilder.AddField(new EmbedFieldBuilder()
            {
                Name = "Professions",
                Value = "\u200b",
                IsInline = true
            });
            
            EmbedFieldBuilder prof1 = new EmbedFieldBuilder() { IsInline = true};
            EmbedFieldBuilder prof2 = new EmbedFieldBuilder() { IsInline = true};
            if(characterSummary.Professions.Count == 2)
            {
                prof1.Name = characterSummary.Professions[0].Name;
                prof2.Name = characterSummary.Professions[1].Name;

                string learned = new string('\u25a0', (int)Math.Floor(Math.Min(450, int.Parse(characterSummary.Professions[0].Skill)) / 450.0 * 15.0));
                string notLearned = new string('\u25a1', 15 - learned.Length);
                prof1.Value = $"{learned}{notLearned} {characterSummary.Professions[0].Skill}/{450}";

                learned = new string('\u25a0', (int)Math.Floor(Math.Min(450, int.Parse(characterSummary.Professions[1].Skill)) / 450.0 * 15.0));
                notLearned = new string('\u25a1', 15 - learned.Length);
                prof2.Value = $"{learned}{notLearned} {characterSummary.Professions[1].Skill}/{450}";
            }
            else if(characterSummary.Professions.Count == 1)
            {
                prof1.Name = characterSummary.Professions[0].Name;
                prof2.Name = "Not learned";

                string learned = new string('\u25a0', (int)Math.Floor(Math.Min(450, int.Parse(characterSummary.Professions[0].Skill)) / 450.0 * 15.0));
                string notLearned = new string('\u25a1', 15 - learned.Length);
                prof1.Value = $"{learned}{notLearned} {characterSummary.Professions[0].Skill}/{450}";
                prof2.Value = $"{new string('\u25a1', 15)} 0/{450}";
            }
            else
            {
                prof1.Name = "Not learned";
                prof2.Name = "Not learned";
                prof1.Value = $"{new string('\u25a1', 15)} 0/{450}";
                prof2.Value = $"{new string('\u25a1', 15)} 0/{450}";
            }

            embedBuilder.AddField(prof1);
            embedBuilder.AddField(prof2);
            
            
            string[] missing = _gscalc.CheckEnchants(items, characterSummary);

            if(missing[0] != string.Empty)
                embedBuilder.AddField(new EmbedFieldBuilder()
                {
                    Name = "Missing gems",
                    Value = missing[0],
                    IsInline = true
                });
            else
                embedBuilder.AddField(new EmbedFieldBuilder()
                {
                    Name = "\u200b",
                    Value = "\u200b",
                    IsInline = true
                });

            if(missing[1] != string.Empty)
                embedBuilder.AddField(new EmbedFieldBuilder()
                {
                    Name = "Missing enchants",
                    Value = missing[1],
                    IsInline = true
                });
            else
                embedBuilder.AddField(new EmbedFieldBuilder()
                {
                    Name = "\u200b",
                    Value = "\u200b",
                    IsInline = true
                });
            
            //dorobit zistovanie PVP itemov a ikonu
            
            embedBuilder.AddField(new EmbedFieldBuilder()
            {
                Name = "Armory",
                Value = $"[{characterSummary.Name}](https://armory.warmane.com/character/{characterSummary.Name}/Icecrown/)",
                IsInline = true
            });

            embedBuilder.WithColor(GearscoreCalculator.GetGearColor(gearScore));

            embedBuilder.ThumbnailUrl = $"attachment://{Path.GetFileName(LookupTables.ClassImages.GetValueOrDefault(characterSummary.ClassMask))}";
            Embed embed = embedBuilder.Build();
            string file = LookupTables.ClassImages.GetValueOrDefault(characterSummary.ClassMask);
            await Context.Channel.SendFileAsync(file, embed: embed);
        }

        [Command("gst")]
        public async Task SummaryAsync(string playerName)
        {
            if(!Regex.IsMatch(playerName, @"^[a-zA-Z]+$"))
            {
                Console.WriteLine($"Invalid character name: {playerName}");
                return;
            }

            StringBuilder warmaneUrl = new StringBuilder();
            warmaneUrl.Append("http://armory.warmane.com/api/character/");
            warmaneUrl.Append(System.Net.WebUtility.UrlEncode(playerName));
            warmaneUrl.Append("/Icecrown/Summary");

            CharacterSummary characterSummary;
            string response = null;
            try
            {
                HttpClient apiClient = new HttpClient();
                apiClient.DefaultRequestHeaders.Accept.Clear();
                apiClient.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                apiClient.DefaultRequestHeaders.Add("User-Agent", ".NET Core Client");
                Task<string> responseTask = apiClient.GetStringAsync(warmaneUrl.ToString());
                response = await responseTask;

                //error response: {"error":"Character does not exist."}

                characterSummary = JsonConvert.DeserializeObject<CharacterSummary>(response);

                if(characterSummary.Error != null)
                {
                    Console.WriteLine($"{characterSummary.Error}");
                    await ReplyAsync($"{characterSummary.Error}");
                    return;
                }
            }
            catch(JsonException ex)
            {
                await ReplyAsync($"Error parsing API data.");
                Console.WriteLine($"Error parsing API data ({nameof(JsonException)}, character name: {playerName}, {ex.Message})");
                return;
            }
            catch(HttpRequestException ex)
            {
                await ReplyAsync($"Error requesting API data.");
                Console.WriteLine($"Error requesting API data ({nameof(HttpRequestException)}, {ex.Message})");
                return;
            }

            int gearScore = _gscalc.CalculateGearscore(characterSummary.Equipment, characterSummary.Class);

            HttpClient client = new HttpClient();
            HtmlDocument doc = new HtmlDocument();
            HttpResponseMessage result;
            try
            {
                result = await client.GetAsync($"http://armory.warmane.com/character/{System.Net.WebUtility.UrlEncode(playerName)}/Icecrown/Summary");
                Stream stream = await result.Content.ReadAsStreamAsync();
                doc.Load(stream);
                //doc.Load(@"C:\Users\Michal\Desktop\html.txt");
            }
            catch(HttpRequestException ex)
            {
                await ReplyAsync($"Error downloading data from armory webpage.");
                Console.WriteLine($"Error downloading armory content. {nameof(HttpRequestException)}: {ex.Message}");
                return;
            }
            string[] rels = null;
            try
            {
                rels =
                    doc.DocumentNode.SelectNodes("//div[contains(concat(' ', @class, ' '), ' item-model ')]//div[contains(concat(' ', @class, ' '), ' item-slot ')]//a[@rel]")
                    .Select(x => x.GetAttributeValue("rel", "")).ToArray();
            }
            catch(ArgumentNullException ex)
            {
                //await ReplyAsync($"Error parsing HTML values {nameof(ArgumentNullException)}");
                Console.WriteLine($"Error parsing HTML values. {nameof(ArgumentNullException)}: {ex.Message}");
                return;
            }
            List<WowItemEnchants> items = new List<WowItemEnchants>();

            foreach(string rel in rels)
            {
                string[] data = rel.Split("&amp;");
                //Console.WriteLine(string.Join(" ", data));

                WowItemEnchants wowItem = new WowItemEnchants();
                wowItem.ItemId = 0;
                wowItem.EnchantId = 0;
                wowItem.TransmogItemId = 0;
                wowItem.Gems = new int[3] { 0, 0, 0 };

                foreach(string item in data)
                {
                    if(item.StartsWith("item="))
                    {
                        wowItem.ItemId = Convert.ToInt32(item.Replace("item=", ""));
                        continue;
                    }
                    if(item.StartsWith("ench="))
                    {
                        wowItem.EnchantId = Convert.ToInt32(item.Replace("ench=", ""));
                        continue;
                    }
                    if(item.StartsWith("transmog="))
                    {
                        wowItem.TransmogItemId = Convert.ToInt32(item.Replace("transmog=", ""));
                        continue;
                    }
                    if(item.StartsWith("gems="))
                    {
                        string[] gems = item.Replace("gems=", "").Split(':');
                        for(int i = 0; i < gems.Length; i++)
                        {
                            wowItem.Gems[i] = Convert.ToInt32(gems[i]);
                        }
                        continue;
                    }
                }
                items.Add(wowItem);
            }

            EmbedBuilder embedBuilder = new EmbedBuilder();

            string[] missing = _gscalc.CheckEnchants(items, characterSummary);

            StringBuilder replyString = new StringBuilder();
            replyString.Append($"Summary for character **{characterSummary.Name}**:\n");
            replyString.Append(string.Format("   **Status**: {0}", characterSummary.Online == true ? "Online\n" : "Offline\n"));
            replyString.Append($"   **Character**: Level {characterSummary.Level} {characterSummary.Race} {characterSummary.Class} - {characterSummary.Faction}\n");
            if(characterSummary.Talents.Count == 1)
            {
                replyString.Append($"   **Spec**: {characterSummary.Talents[0].Tree}\n");
            }
            else if(characterSummary.Talents.Count == 2)
            {
                replyString.Append($"   **Specs**: {characterSummary.Talents[0].Tree} and {characterSummary.Talents[1].Tree}\n");
            }
            replyString.Append($"   **Guild**: {characterSummary.Guild}\n");
            replyString.Append($"   **Achievement points**: {characterSummary.AchievementPoints}\n");
            replyString.Append($"   **Gearscore**: {gearScore}\n");
            replyString.Append($"   **Honorable kills**: {characterSummary.HonorableKills}\n");


            if(characterSummary.Professions.Count == 1)
            {
                replyString.Append($"   **Profession**: {characterSummary.Professions[0].Skill} {characterSummary.Professions[0].Name}\n");
            }
            else if(characterSummary.Professions.Count == 2)
            {
                replyString.Append($"   **Professions**: {characterSummary.Professions[0].Skill} {characterSummary.Professions[0].Name} " +
                    $"and {characterSummary.Professions[1].Skill} {characterSummary.Professions[1].Name}\n");
            }

            if(missing[0] != string.Empty)
            {
                replyString.Append($"   **Missing gems**: {missing[0]}\n");
            }
            if(missing[1] != string.Empty)
            {
                replyString.Append($"   **Missing enchants**: {missing[1]}\n");
            }
            replyString.Append($"   **Armory link**: http://armory.warmane.com/character/{characterSummary.Name}/Icecrown/");

            await ReplyAsync(replyString.ToString());
        }
    }
}
