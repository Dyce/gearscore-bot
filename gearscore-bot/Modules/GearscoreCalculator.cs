﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Discord.Commands;
using gearscore_bot.Model;
using gearscore_bot.Model.Database;
using gearscore_bot.Model.Website;
using Microsoft.EntityFrameworkCore;

namespace gearscore_bot.Modules
{
    public class GearscoreCalculator : ModuleBase<SocketCommandContext>
    {
        private DataTable _tokens;
        private Dictionary<int, float> _slotModifiers;
        private Dictionary<int, RandomNumbers> _tableA;
        private Dictionary<int, RandomNumbers> _tableB;
        public GearscoreCalculator()
        {
            _tokens = new DataTable();
            _slotModifiers = new Dictionary<int, float>()
            {
                {1, 1.0f }, //head
                {2, 0.5625f }, //neck
                {3, 0.75f }, //shoulder
                {5, 1.0f }, //chest
                {6, 0.75f }, //waist
                {7, 1.0f }, //legs
                {8, 0.75f }, //feet
                {9, 0.5625f }, //wrist
                {10, 0.75f }, //hand
                {11, 0.5625f }, //finger
                {12,  0.5625f}, //trinket
                {13, 1.0f }, //weapon
                {14, 1.0f }, //shield
                {15, 0.3164f }, //ranged
                {16,  0.5625f}, //back
                {17, 2.0f }, //two-hand
                {20, 1.0f },//robe
                {21, 1.0f }, //main-hand w
                {22, 1.0f }, //off-hand w
                {23, 1.0f }, //holdable
                {25, 0.3164f }, //thrown
                {26, 0.3164f }, //ranged right
                {28, 0.3164f },//relic

            };

            _tableA = new Dictionary<int, RandomNumbers>()
            {
                {2, new RandomNumbers(73.0f, 1.0f)},
                {3, new RandomNumbers(81.3750f, 0.8125f) },
                {4, new RandomNumbers(91.45f, 0.65f) }
            };

            _tableB = new Dictionary<int, RandomNumbers>()
            {
                {1, new RandomNumbers(0.0f, 2.25f)},
                {2, new RandomNumbers(8.0f, 2.0f)},
                {3, new RandomNumbers(0.75f, 1.8f) },
                {4, new RandomNumbers(26.0f, 1.2f) }
            };

            _tokens.Columns.Add("id", typeof(int));
            _tokens.Columns.Add("item_score", typeof(int));
            _tokens.Columns.Add("sub_type", typeof(string));
            _tokens.Columns.Add("item_slot", typeof(int));
            _tokens.Columns.Add("item_level", typeof(int));

            //T7
            _tokens.Rows.Add(40618, 310, "Vanquisher", 1, 200);
            _tokens.Rows.Add(40616, 310, "Conqueror", 1, 200);
            _tokens.Rows.Add(40617, 310, "Protector", 1, 200);
            _tokens.Rows.Add(40615, 233, "Vanquisher", 10, 200);
            _tokens.Rows.Add(40613, 233, "Conqueror", 10, 200);
            _tokens.Rows.Add(40614, 233, "Protector", 10, 200);
            _tokens.Rows.Add(40612, 310, "Vanquisher", 5, 200);
            _tokens.Rows.Add(40610, 310, "Conqueror", 5, 200);
            _tokens.Rows.Add(40611, 310, "Protector", 5, 200);
            _tokens.Rows.Add(40624, 233, "Vanquisher", 3, 200);
            _tokens.Rows.Add(40622, 233, "Conqueror", 3, 200);
            _tokens.Rows.Add(40623, 233, "Protector", 3, 200);
            _tokens.Rows.Add(40621, 310, "Vanquisher", 7, 200);
            _tokens.Rows.Add(40619, 310, "Conqueror", 7, 200);
            _tokens.Rows.Add(40620, 310, "Protector", 7, 200);

            //T7.5
            _tokens.Rows.Add(40633, 348, "Vanquisher", 1, 213);
            _tokens.Rows.Add(40631, 348, "Conqueror", 1, 213);
            _tokens.Rows.Add(40632, 348, "Protector", 1, 213);
            _tokens.Rows.Add(40630, 261, "Vanquisher", 10, 213);
            _tokens.Rows.Add(40628, 261, "Conqueror", 10, 213);
            _tokens.Rows.Add(40629, 261, "Protector", 10, 213);
            _tokens.Rows.Add(40627, 348, "Vanquisher", 5, 213);
            _tokens.Rows.Add(40625, 348, "Conqueror", 5, 213);
            _tokens.Rows.Add(40626, 348, "Protector", 5, 213);
            _tokens.Rows.Add(40639, 261, "Vanquisher", 3, 213);
            _tokens.Rows.Add(40637, 261, "Conqueror", 3, 213);
            _tokens.Rows.Add(40638, 261, "Protector", 3, 213);
            _tokens.Rows.Add(40636, 348, "Vanquisher", 7, 213);
            _tokens.Rows.Add(40634, 348, "Conqueror", 7, 213);
            _tokens.Rows.Add(40635, 348, "Protector", 7, 213);

            //T8
            _tokens.Rows.Add(45649, 365, "Vanquisher", 1, 219);
            _tokens.Rows.Add(45647, 365, "Conqueror", 1, 219);
            _tokens.Rows.Add(45648, 365, "Protector", 1, 219);
            _tokens.Rows.Add(45646, 274, "Vanquisher", 10, 219);
            _tokens.Rows.Add(45644, 274, "Conqueror", 10, 219);
            _tokens.Rows.Add(45645, 274, "Protector", 10, 219);
            _tokens.Rows.Add(45637, 365, "Vanquisher", 5, 219);
            _tokens.Rows.Add(45635, 365, "Conqueror", 5, 219);
            _tokens.Rows.Add(45636, 365, "Protector", 5, 219);
            _tokens.Rows.Add(45661, 274, "Vanquisher", 3, 219);
            _tokens.Rows.Add(45659, 274, "Conqueror", 3, 219);
            _tokens.Rows.Add(45660, 274, "Protector", 3, 219);
            _tokens.Rows.Add(45652, 365, "Vanquisher", 7, 219);
            _tokens.Rows.Add(45650, 365, "Conqueror", 7, 219);
            _tokens.Rows.Add(45651, 365, "Protector", 7, 219);

            //T8.5
            _tokens.Rows.Add(45640, 385, "Vanquisher", 1, 226);
            _tokens.Rows.Add(45638, 385, "Conqueror", 1, 226);
            _tokens.Rows.Add(45639, 385, "Protector", 1, 226);
            _tokens.Rows.Add(45643, 289, "Vanquisher", 10, 226);
            _tokens.Rows.Add(45641, 289, "Conqueror", 10, 226);
            _tokens.Rows.Add(45642, 289, "Protector", 10, 226);
            _tokens.Rows.Add(45632, 385, "Conqueror", 5, 226);
            _tokens.Rows.Add(45633, 385, "Protector", 5, 226);
            _tokens.Rows.Add(45634, 385, "Vanquisher", 5, 226);
            _tokens.Rows.Add(45658, 289, "Vanquisher", 3, 226);
            _tokens.Rows.Add(45656, 289, "Conqueror", 3, 226);
            _tokens.Rows.Add(45657, 289, "Protector", 3, 226);
            _tokens.Rows.Add(45655, 385, "Vanquisher", 7, 226);
            _tokens.Rows.Add(45653, 385, "Conqueror", 7, 226);
            _tokens.Rows.Add(45654, 385, "Protector", 7, 226);
        }

        public static Discord.Color GetGearColor(int gearScore)
        {
            int index = gearScore / 1000;
            float red = LookupTables.GearscoreColors[index].Red.Base +
                LookupTables.GearscoreColors[index].Red.Bias * (gearScore - index * 1000);
            float green = LookupTables.GearscoreColors[index].Green.Base +
                LookupTables.GearscoreColors[index].Green.Bias * (gearScore - index * 1000);
            float blue = LookupTables.GearscoreColors[index].Blue.Base +
                LookupTables.GearscoreColors[index].Blue.Bias * (gearScore - index * 1000);

            return new Discord.Color(red, green, blue);
        }

        struct RandomNumbers
        {
            public RandomNumbers(float a, float b)
            {
                randomNumberA = a;
                randomNumberB = b;
            }
            public float randomNumberA;
            public float randomNumberB;
        }

        public string[] CheckEnchants(List<WowItemEnchants> equipment, CharacterSummary character)
        {
            List<string> missingGems = new List<string>();
            List<string> missingEnchants = new List<string>();

            int[] enchantableSlots = { 1, 3, 5, 7, 8, 9, 10, 13, 14, 15, 16, 17, 20, 21, 22 };

            using(WarcraftContext ctx = new WarcraftContext())
            {
                foreach(var equip in equipment)
                {
                    WowItem item = ctx.WowItems.Include(i => i.InventorySlot).Single(i => i.EntryId == equip.ItemId);
                    int count = item.FirstSocketType != 0 ? 1 : 0;
                    count = item.SecondSocketType != 0 ? count + 1 : count;
                    count = item.ThirdSocketType != 0 ? count + 1 : count;
                    count = item.InventorySlot.Id == 6 ? count + 1 : count;

                    if(equip.Gems.Where(g => g != 0).Count() < count)
                    {
                        missingGems.Add(item.InventorySlot.Name);
                    }

                    if(item.InventorySlot.Id == 26 && item.ClassId == 2 && character.Class == "Hunter") //ranged right
                    {
                        if((item.SubclassId == 2 || item.SubclassId == 3 || item.SubclassId == 18) && equip.EnchantId == 0)
                            missingEnchants.Add(item.InventorySlot.Name);
                        continue;
                    }

                    if(enchantableSlots.Contains((int)item.InventoryTypeId) && equip.EnchantId == 0)
                    {
                        missingEnchants.Add(item.InventorySlot.Name);
                    }
                }
                return new string[] { string.Join(", ", missingGems), string.Join(", ", missingEnchants) };
            }
        }

        public int CalculateGearscore(List<Equipment> equipment, string playerClass)
        {
            List<WowItem> items = new List<WowItem>();
            float finalScore = 0;
            float titansGripModifier = 1.0f;
            float tempscore;
            bool isHunter = playerClass == "Hunter";

            using(WarcraftContext ctx = new WarcraftContext())
            {
                foreach(var item in equipment)
                {
                    items.Add(ctx.WowItems.Include(p => p.InventorySlot).Single(i => i.EntryId == Convert.ToInt32(item.Item)));
                }
                if(items.Where(i => i.InventoryTypeId == 17).Count() == 2)
                {
                    titansGripModifier = 0.5f;
                }

                foreach(var item in items)
                {
                    tempscore = GetItemScore(item.EntryId, Convert.ToInt32(item.ItemLevel), (int)item.QualityId, (int)item.InventoryTypeId);
                    if(isHunter)
                    {
                        if(item.InventoryTypeId == 13 || item.InventoryTypeId == 17 || item.InventoryTypeId == 22)
                            tempscore *= 0.3164f;
                        else if(item.InventoryTypeId == 26 || item.InventoryTypeId == 15)
                            tempscore *= 5.3224f;
                    }
                    else if(item.InventoryTypeId == 17)
                        tempscore *= titansGripModifier;
                    finalScore += tempscore;
                }
            }
            return (int)finalScore;
        }

        private float GetItemScore(int itemId, int itemLevel, int rarity, int equipSlot)
        {
            DataRow result = _tokens.Rows.Cast<DataRow>().SingleOrDefault(r => (int)r["id"] == itemId);
            if(result != null)
            {
                return (int)result["item_score"];
            }

            float qualityScale = 1;
            float itemScore = 0;
            float scale = 1.8618f;
            float ilvl = itemLevel;

            if(rarity == 5)
            {
                qualityScale = 1.3f;
                rarity = 4;
            }
            else if(rarity == 0 || rarity == 1)
            {
                qualityScale = 0.005f;
                rarity = 2;
            }
            else if(rarity == 7)
            {
                ilvl = 187.05f;
                rarity = 3;
            }

            if(_slotModifiers.ContainsKey(equipSlot))
            {
                Dictionary<int, RandomNumbers> table;
                if(itemLevel > 120)
                    table = _tableA;
                else
                    table = _tableB;
                if(rarity >= 2 && rarity <= 4)
                {
                    itemScore = (((ilvl - table[rarity].randomNumberA) / table[rarity].randomNumberB) * _slotModifiers[equipSlot] * scale * qualityScale);
                    if(itemScore < 0)
                        itemScore = 0;
                }
            }
            return (float)Math.Floor(itemScore);
        }
    }
}
