﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
//using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using Discord;
using Discord.WebSocket;
using gearscore_bot.Model;
using gearscore_bot.Model.Database;
using gearscore_bot.Model.Website;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace gearscore_bot.Modules
{
    public class GuildLog
    {
        public GuildLog(DiscordSocketClient client, CancellationToken cancellationToken)
        {
            _token = cancellationToken;
            _discordClient = client;
            _client = new HttpClient();
            //_task = Task.Run(ArmoryPolling, _token);
            StartArmoryPolling();
            //TestNotification();
        }

        private async void TestNotification()
        {
            GuildMember player = new GuildMember()
            {
                Class = "Botadin",
                ClassMask = 128,
                Level = 42,
                Race = "Murloc",
                Name = "Botík"
            };
            EventType et = new EventType()
            {
                Id = 1,
                Name = "Joined guild"
            };
            EventType et2 = new EventType()
            {
                Id = 2,
                Name = "Left guild"
            };
            GuildLogEntry entry = new GuildLogEntry()
            {
                EventType = et,
                Member = player,
                Timestamp = DateTime.Now.AddDays(-17)
            };
            GuildLogEntry entry2 = new GuildLogEntry()
            {
                EventType = et2,
                Member = player,
                Timestamp = DateTime.Now.AddDays(-13)
            };
            GuildLogEntry entry3 = new GuildLogEntry()
            {
                EventType = et,
                Member = player,
                Timestamp = DateTime.Now.AddDays(-5)
            };
            GuildLogEntry entry4 = new GuildLogEntry()
            {
                EventType = et2,
                Member = player,
                Timestamp = DateTime.Now.AddDays(-3)
            };
            GuildLogEntry entry5 = new GuildLogEntry()
            {
                EventType = et,
                Member = player,
                Timestamp = DateTime.Now
            };

            player.Logs = new List<GuildLogEntry>()
            {
                entry2, entry3, entry, entry4, entry5
            };
            
            while(_discordClient.ConnectionState != ConnectionState.Connected)
            {
                Console.WriteLine("Not connected to discord, waiting...");
                await Task.Delay(5000, _token);
            }
            _messageChannel = _discordClient.GetGuild(758050738026053743).GetTextChannel(851198676730380368);

            await SendNotification(player, player.Logs);
        }

        private CancellationToken _token;
        private DiscordSocketClient _discordClient;
        private IMessageChannel _messageChannel;
        private Task _task;
        private HttpClient _client;

        private async void StartArmoryPolling()
        {
            _task = await Task.Run(ArmoryPolling, _token).ContinueWith(async t =>
            {
                if(t.IsFaulted)
                {
                    await _discordClient.SetActivityAsync(new Game($"\uD83D\uDC80 since {DateTime.Now.ToLocalTime():g}"));
                    foreach(var exception in t.Exception?.Flatten().InnerExceptions)
                    {
                        Exception ex = exception;
                        while(ex != null)
                        {
                            Console.WriteLine(ex.Message ?? "");
                            ex = ex.InnerException;
                        }
                    }
                }
            });
        }

        private async Task ArmoryPolling()
        {
            using(GuildContext ctx = new GuildContext())
            {
                await ctx.Database.MigrateAsync();
            }
            await Task.Delay(10000, _token);
            while(_discordClient.ConnectionState != ConnectionState.Connected)
            {
                Console.WriteLine("Not connected to discord, waiting...");
                await Task.Delay(5000, _token);
            }

            _messageChannel = _discordClient.GetGuild(758050738026053743).GetTextChannel(851198676730380368);

            Console.WriteLine("Starting armory polling.");

            if(_messageChannel == null)
            {
                Console.WriteLine("ERROR: Message channel is empty.");
                return;
            }
            HttpResponseMessage httpMessage = null;
            
            while(!_token.IsCancellationRequested)
            {
                httpMessage = await _client.GetAsync(@"http://armory.warmane.com/api/guild/CzechoSlovakiaGuild/Icecrown/summary");

                if(!httpMessage.IsSuccessStatusCode)
                {
                    Console.WriteLine($"ERROR: Received status code {httpMessage.StatusCode} ({httpMessage.ReasonPhrase})");
                    await Task.Delay(600000, _token);
                    continue;
                }

                string responseMessage = await httpMessage.Content.ReadAsStringAsync();
                GuildSummary summary = JsonConvert.DeserializeObject<GuildSummary>(responseMessage);

                if(summary.Error != null)
                {
                    Console.WriteLine($"ERROR: {summary.Error}. Retrying in 2 minutes.");
                    await Task.Delay(1000 * 60 * 2, _token);
                    continue;
                }

                using(GuildContext ctx = new GuildContext())
                {
                    EventType JoinedType = await ctx.EventTypes.AsQueryable().SingleAsync(e => e.Id == 1);
                    EventType LeftType = await ctx.EventTypes.AsQueryable().SingleAsync(e => e.Id == 2);
                    bool isEmpty = !await ctx.Members.AsQueryable().AnyAsync();
                    //ak je prazdna databaza
                    if(isEmpty)
                    {
                        Console.WriteLine("Database is empty, importing data");
                        foreach(var member in summary.Roster)
                        {
                            GuildMember Member = new GuildMember()
                            {
                                Achievementpoints = int.Parse(member.Achievementpoints),
                                Class = member.Class,
                                ClassMask = member.ClassMask,
                                Gender = member.Gender,
                                IsMember = true,
                                Level = int.Parse(member.Level),
                                Name = member.Name,
                                Race = member.Race,
                                RaceMask = member.RaceMask
                            };

                            GuildLogEntry logEntry = new GuildLogEntry()
                            {
                                Member = Member,
                                EventType = JoinedType
                            };
                            Member.Logs = new List<GuildLogEntry>()
                            {
                                logEntry
                            };

                            ctx.Add(Member);
                            ctx.Add(logEntry);
                        }

                        try
                        {
                            int result = await ctx.SaveChangesAsync();
                            Console.WriteLine($"Imported {result} rows.");
                            if(result == 0)
                            {
                                await _messageChannel.SendMessageAsync("ERROR: Cannot insert data to empty database.");
                                return;
                            }
                        }
                        catch(Exception ex)
                        {
                            await _messageChannel.SendMessageAsync($"ERROR: Database exception ({ex.GetType()}).");
                            Console.WriteLine($"ERROR: Database exception ({ex.GetType()})\n{ex.Message}.");
                            return;
                        }
                    }
                    //zistujem zmeny
                    else
                    {
                        //prechadzam roster a hladam novych ludi v guilde
                        foreach(var member in summary.Roster)
                        {
                            GuildMember Member = await ctx.Members.Include(m => m.Logs)
                                .ThenInclude(l => l.EventType)
                                .SingleOrDefaultAsync(m => m.Name == member.Name);
                            //hraca uz mam v databaze, t.j. aktualne je clenom alebo bol predtym
                            if(Member != null)
                            {
                                //aktualizujem udaje o nom
                                Member.Achievementpoints = int.Parse(member.Achievementpoints);
                                Member.Class = member.Class;
                                Member.ClassMask = member.ClassMask;
                                Member.Gender = member.Gender;
                                Member.Level = int.Parse(member.Level);
                                Member.Race = member.Race;
                                Member.RaceMask = member.RaceMask;
                                
                                //je to "novy" clen, ktory uz predtym u nas bol
                                if(!Member.IsMember)
                                {
                                    Member.IsMember = true;
                                    GuildLogEntry logEntry = new GuildLogEntry()
                                    {
                                        EventType = JoinedType,
                                        Member = Member,
                                        Timestamp = DateTime.Now
                                    };
                                    await ctx.Logs.AddAsync(logEntry);
                                    Member.Logs.Add(logEntry);
                                    await SendNotification(Member, Member.Logs);
                                    logEntry.Timestamp = DateTime.MinValue;
                                }
                            }
                            //nemam ho v databaze, cize je to uplne novy clen
                            else
                            {
                                Member = new GuildMember()
                                {
                                    Achievementpoints = int.Parse(member.Achievementpoints),
                                    Class = member.Class,
                                    ClassMask = member.ClassMask,
                                    Gender = member.Gender,
                                    Level = int.Parse(member.Level),
                                    Name = member.Name,
                                    Race = member.Race,
                                    RaceMask = member.RaceMask,
                                    IsMember = true
                                };
                                GuildLogEntry entry = new GuildLogEntry()
                                {
                                    Member = Member,
                                    EventType = JoinedType,
                                    Timestamp = DateTime.Now
                                };
                                Member.Logs = new List<GuildLogEntry>()
                                {
                                    entry
                                };
                                await ctx.Members.AddAsync(Member);
                                await ctx.Logs.AddAsync(entry);
                                await SendNotification(Member, Member.Logs);
                                entry.Timestamp = DateTime.MinValue;
                            }
                        }

                        //ulozim novych clenov do databazy
                        try
                        {
                            await ctx.SaveChangesAsync();
                        }
                        catch(Exception ex)
                        {
                            await _messageChannel.SendMessageAsync($"ERROR: Database exception ({ex.GetType()}).");
                            Console.WriteLine($"ERROR: Database exception ({ex.GetType()})\n{ex.Message}.");
                            return;
                        }

                        //idem zistovat, ci z guildy niekto neodisiel
                        //zoberiem si mena postav, ktore su aktualne v guilde
                        var apiroster = summary.Roster.Select(p => p.Name);
                        
                        //z databazy si zoberiem postavy, o ktorych mam poznacene ze su v guilde
                        //a zaroven sa nenachadzaju v zozname ziskanom z API
                        //vznikne zoznam postav, ktore uz nie su v guilde, ale v databaze su stale povazovani za clenov
                        var diff = await ctx.Members.Include(m => m.Logs)
                            .ThenInclude(l => l.EventType)
                            .Where(m => m.IsMember && !apiroster.Contains(m.Name))
                            .ToListAsync();

                        EventType type = await ctx.EventTypes.AsQueryable().SingleAsync(e => e.Id == 2);
                        //prechadzam ziskany zoznam a rusim clenstvo v guilde + pridavam zaznam
                        for(int i = 0; i < diff.Count; i++)
                        {
                            diff[i].IsMember = false;
                            GuildLogEntry logEntry = new GuildLogEntry()
                            {
                                EventType = type,
                                Member = diff[i],
                                Timestamp = DateTime.Now
                            };
                            diff[i].Logs.Add(logEntry);
                            ctx.Logs.Add(logEntry);
                            await SendNotification(diff[i], diff[i].Logs);
                            logEntry.Timestamp = DateTime.MinValue;
                        }
                        int res = await ctx.SaveChangesAsync();
                    }
                }
                await _discordClient.SetActivityAsync(new Game($"Guild log ({DateTime.Now.ToLocalTime():g})", ActivityType.Watching));
                await Task.Delay(1000 * 60 * 30, _token);
            }
        }

        private async Task SendNotification(GuildMember player, List<GuildLogEntry> log)
        {
            EmbedBuilder eb = new EmbedBuilder();
            EventType lastEventType = log.OrderByDescending(l => l.Timestamp).FirstOrDefault()?.EventType;
            if(lastEventType == null)
            {
                await _messageChannel.SendMessageAsync("Notification error.");
                Console.WriteLine($"ERROR: {nameof(SendNotification)} (Player: {player.Name}) ");
                return;
            }
            eb.WithColor(lastEventType.Id == 1 ? LookupTables.ClassColors.GetValueOrDefault(player.ClassMask) : new Color(255, 70, 70));
            eb.WithTitle($"{player.Name} has {(lastEventType.Id == 1 ? "joined" : "left")} the guild!");
            eb.AddField(new EmbedFieldBuilder() 
            { 
                Name = "Details", 
                Value = $"Level {player.Level} {player.Race} {player.Class}", 
                IsInline = true 
            });

            if(lastEventType.Id == 1 && log.Count > 2)
            {
                var preiousMembership = log.OrderByDescending(l => l.Timestamp).Skip(1).Take(2);
                var previousFrom = preiousMembership.Single(l => l.EventType.Id == 1).Timestamp;
                var previousTo = preiousMembership.Single(l => l.EventType.Id == 2).Timestamp;
                
                eb.AddField(new EmbedFieldBuilder()
                {
                    Name = "Previous membership",
                    Value = $"{previousFrom.ToString("d.M.yyyy")} - " +
                        $"{previousTo.ToString("d.M.yyyy")} " +
                        $"({(previousTo - previousFrom).Days} days)",
                    IsInline = true
                });
            }
            else if(lastEventType.Id == 2)
            {
                EmbedFieldBuilder efb = new EmbedFieldBuilder();
                efb.Name = "Member since";
                efb.IsInline = true;

                DateTime? joinedDate = log.Where(l => l.EventType.Id == 1).OrderByDescending(l => l.Timestamp).FirstOrDefault().Timestamp;
                if(!joinedDate.HasValue)
                    efb.Value = "Error: Cannot find guild joined date.";
                else
                    efb.Value = $"{joinedDate.Value.ToString("d.M.yyyy")} ({(DateTime.Now - joinedDate.Value).Days} days)";


                eb.AddField(efb);
            }
            
            eb.AddField(new EmbedFieldBuilder() 
            { 
                Name = "Armory", 
                Value = $"[{player.Name}](https://armory.warmane.com/character/{player.Name}/Icecrown/)", 
                IsInline = true 
            });

            eb.ThumbnailUrl = $"attachment://{Path.GetFileName(LookupTables.ClassImages.GetValueOrDefault(player.ClassMask))}";
            eb.WithCurrentTimestamp();
            
            await _messageChannel.SendFileAsync(LookupTables.ClassImages.GetValueOrDefault(player.ClassMask), embed: eb.Build());
            //await _messageChannel.SendMessageAsync(embed: eb.Build());
        }
    }
}
